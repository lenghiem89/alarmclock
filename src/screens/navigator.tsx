import React from 'react';
import {View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {createAppContainer} from 'react-navigation';

import {C1lt} from '../contents/lyThuyet/c1lt';
import {C2lt} from '../contents/lyThuyet/c2lt';
import {C3lt} from '../contents/lyThuyet/c3lt';
import {C4lt} from '../contents/lyThuyet/c4lt';
import {C5lt} from '../contents/lyThuyet/c5lt';
import {C6lt} from '../contents/lyThuyet/c6lt';
import {C7lt} from '../contents/lyThuyet/c7lt';
import {C8lt} from '../contents/lyThuyet/c8lt';
import {C9lt} from '../contents/lyThuyet/c9lt';
import C0lt from '../contents/lyThuyet/c0lt';

import {C1tn} from '../contents/tracNghiem/c1tn';
import C0tn from '../contents/tracNghiem/c0tn';

import LtNavigator from '../contents/lyThuyet/ltnavigator';

// const TNnavigator = createStackNavigator({
//   'TRẮC NGHIỆM': {
//     screen: C0tn,
//   },
// });

const Tab = createBottomTabNavigator();

const KtcNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Ly Thuyet" component={LtNavigator} />
      <Tab.Screen name="Trac Nghiem" component={C0tn} />
    </Tab.Navigator>
  );
};

export default KtcNavigator;

// const KtcNavigator = createBottomTabNavigator({
//   LT: Ltnavigator,
//   TN: TNnavigator,
// });
// const KtcNavigator = createBottomTabNavigator({
//   LT: Ltnavigator,
//   TN: TNnavigator,
// });
