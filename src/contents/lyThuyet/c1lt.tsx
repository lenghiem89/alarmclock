import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const C1lt = () => {
  return (
    <View>
      <Text style={styles.body}>Đây là nội dung chương 1</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
  },
  body: {
    fontSize: 16,
    marginLeft: 20,
    fontStyle: 'italic',
  },
});

export {C1lt};

/*<Text style={styles.header}>KIẾN THỨC CHUNG</Text>
      <Text style={styles.header}>CHƯƠNG 1</Text>
      <Text> </Text>*/
