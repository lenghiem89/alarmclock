import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const C0lt = props => {
  return (
    <View>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate('CHUONG1');
        }}>
        <Text style={styles.buttontext}>Chương 1</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG 2'});
        }}>
        <Text style={styles.buttontext}>Chương 2</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG 3'});
        }}>
        <Text style={styles.buttontext}>Chương 3</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG 4'});
        }}>
        <Text style={styles.buttontext}>Chương 4</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG 5'});
        }}>
        <Text style={styles.buttontext}>Chương 5</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG 6'});
        }}>
        <Text style={styles.buttontext}>Chương 6</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG 7'});
        }}>
        <Text style={styles.buttontext}>Chương 7</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG 8'});
        }}>
        <Text style={styles.buttontext}>Chương 8</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG 9'});
        }}>
        <Text style={styles.buttontext}>Chương 9</Text>
      </TouchableOpacity>
    </View>
  );
};

export default C0lt;

const styles = StyleSheet.create({
  textlink: {
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
  buttontext: {
    color: 'blue',
    fontSize: 24,
  },
});
