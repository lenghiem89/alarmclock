import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const C2lt = () => {
  return (
    <View>
      <Text style={styles.body}>Đây là nội dung chương 2</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
  },
  body: {
    fontSize: 16,
    marginLeft: 20,
    fontStyle: 'italic',
  },
});

export {C2lt};

/*<Text style={styles.header}>KIẾN THỨC CHUNG</Text>
      <Text style={styles.header}>CHƯƠNG 2</Text>
      <Text> </Text>*/
