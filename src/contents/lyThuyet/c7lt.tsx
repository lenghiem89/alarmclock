import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const C7lt = () => {
  return (
    <View>
      <Text style={styles.body}>Đây là nội dung chương 7</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
  },
  body: {
    fontSize: 16,
    marginLeft: 20,
    fontStyle: 'italic',
  },
});

export {C7lt};
