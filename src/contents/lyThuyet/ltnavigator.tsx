// In App.js in a new project

import * as React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import C0lt from './c0lt';
import {C1lt} from './c1lt';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Stack = createStackNavigator();

function LtNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Chuong 0" component={C0lt} />
      <Stack.Screen name="CHUONG1" component={C1lt} />
    </Stack.Navigator>
  );
}

const Drawer = createDrawerNavigator();

export default function KtcDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="LtNavigator" component={LtNavigator} />
      <Drawer.Screen name="Chuong 0" component={C0lt} />
      <Drawer.Screen name="CHUONG1" component={C1lt} />
    </Drawer.Navigator>
  );
}
