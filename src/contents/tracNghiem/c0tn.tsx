import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const C0tn = props => {
  return (
    <View>
      <TouchableOpacity
        style={styles.textlink}
        onPress={() => {
          props.navigation.navigate({routeName: 'CHƯƠNG1'});
        }}>
        <Text style={styles.buttontext}>Chương 1</Text>
      </TouchableOpacity>
    </View>
  );
};

export default C0tn;

const styles = StyleSheet.create({
  textlink: {
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
  buttontext: {
    color: 'blue',
    fontSize: 24,
  },
});
