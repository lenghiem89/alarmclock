import React from 'react';
import {NavigationContainer} from '@react-navigation/native';

import Ktcnavigator from './src/screens/navigator';

const App = () => {
  return <Ktcnavigator />;
};

export default App;

/*    const [value, setValue] = useState<string>();
     const myAlert = () => {
       Alert.alert(`Value ${value} da duoc ghi nhan`);
       console.log(value);
     };
     return (
       <SafeAreaView style={styles.container}>
         <TextInput
           value={value}
           onChangeText={setValue}
           style={styles.textInput}
         />
         <TouchableOpacity onPress={myAlert} style={styles.button}>
           <Text>Submit</Text>
         </TouchableOpacity>
       </SafeAreaView>
     );
*/
// import {C1lt} from './src/contents/lyThuyet/index';
/*
const styles = StyleSheet.create({
    container: {
      flex: 1,
      margin: 20,
    },
    loginTitle: {
      fontSize: 30,
      textAlign: 'center',
      color: 'green',
      fontWeight: '800',
      marginVertical: 50,
    },
    textInput: {
      height: 50,
      borderColor: 'gray',
      borderRadius: 10,
      borderWidth: 1,
      paddingHorizontal: 10,
    },
    button: {
      padding: 20,
      borderRadius: 10,
      borderColor: 'black',
      borderWidth: 1,
      backgroundColor: 'purple',
    },
  }); */

// import * as React from 'react';
// import {View, Text, TouchableOpacity} from 'react-native';
// import {NavigationContainer} from '@react-navigation/native';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// function HomeScreen() {
//   return (
//     <View>
//       <Text>Home!</Text>
//     </View>
//   );
// }

// function SettingsScreen() {
//   return (
//     <View>
//       <Text>Settings!</Text>
//     </View>
//   );
// }

// function MyTabBar(state, descriptors, navigation) {
//   return (
//     <View style={{flexDirection: 'row'}}>
//       {state.routes.map((route, index) => {
//         const {options} = descriptors[route.key];
//         const label =
//           options.tabBarLabel !== undefined
//             ? options.tabBarLabel
//             : options.title !== undefined
//             ? options.title
//             : route.name;

//         const isFocused = state.index === index;

//         const onPress = () => {
//           const event = navigation.emit({
//             type: 'tabPress',
//             target: route.key,
//           });

//           if (!isFocused && !event.defaultPrevented) {
//             navigation.navigate(route.name);
//           }
//         };

//         const onLongPress = () => {
//           navigation.emit({
//             type: 'tabLongPress',
//             target: route.key,
//           });
//         };

//         return (
//           <TouchableOpacity
//             accessibilityRole="button"
//             accessibilityState={isFocused ? {selected: true} : {}}
//             accessibilityLabel={options.tabBarAccessibilityLabel}
//             testID={options.tabBarTestID}
//             onPress={onPress}
//             onLongPress={onLongPress}
//             style={{flex: 1}}>
//             <Text style={{color: isFocused ? '#673ab7' : '#222'}}>{label}</Text>
//           </TouchableOpacity>
//         );
//       })}
//     </View>
//   );
// }

// const Tab = createBottomTabNavigator();

// export default function App() {
//   return (
//     <NavigationContainer>
//       <Tab.Navigator tabBar={props => <MyTabBar {...props} />}>
//         <Tab.Screen name="Home" component={HomeScreen} />
//         <Tab.Screen name="Settings" component={SettingsScreen} />
//       </Tab.Navigator>
//     </NavigationContainer>
//   );
// }

// import {createAppContainer} from '@react-navigation/native';
// const LTscreen = () => {
//   return <Ktcnavigator />;
// };

// const TNscreen = () => {
//   return <TNnavigator />;
// };

// const Ktcscreen = () => {
//   return <Ktcnavigator />;
// };
