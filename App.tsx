import React from 'react';
import {NavigationContainer} from '@react-navigation/native';

import KtcNavigator from './src/screens/navigator';

const App = () => {
  return (
    <NavigationContainer>
      <KtcNavigator />
    </NavigationContainer>
  );
};

export default App;
